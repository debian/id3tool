id3tool (1.2a-14) UNRELEASED; urgency=medium

  [ Debian Janitor ]
  * Fix day-of-week for changelog entry 1.1b.

  [ Peter Pentchev ]
  * Declare compliance with Debian Policy 4.7.0 with no changes.
  * Add the year 2024 to my debian/* copyright notice.
  * Use dh-package-notes to record ELF metadata.
  * Use dpkg's default.mk for completeness.
  * Declare dpkg-build-api v1, drop the implied Rules-Requires-Root: no.
  * Use debhelper compat level 14:
    - use X-DH-Compat.
    - let debhelper take care of some default dependencies
  * Use debputy's X-Style: black.

 -- Peter Pentchev <roam@debian.org>  Wed, 28 Aug 2024 22:05:45 +0300

id3tool (1.2a-13) unstable; urgency=medium

  * Declare compliance with Debian Policy 4.6.0 with no changes.
  * Add the year 2021 to my debian/* copyright notice.
  * Explicitly declare dh-sequence-single-binary as a virtual
    build dependency.
  * Use the Perl Audio::Scan module instead of the deprecated MP3::Info
    one in the autopkgtest.

 -- Peter Pentchev <roam@debian.org>  Fri, 31 Dec 2021 23:55:19 +0200

id3tool (1.2a-12) unstable; urgency=medium

  * Disable the "stringop-truncation" GCC warning - our use of
    strncpy() is correct.
  * Add the year 2020 to my debian/* copyright notice.
  * Declare compliance with Debian Policy 4.5.0 with no changes.
  * Drop the Name and Contact upstream metadata fields.
  * Move away from git-dpm.
  * Add a test suite, run it via autopkgtest, too.
  * Bump the debhelper compat level to 13 with no changes.
  * Add a trivial git-buildpackage config file.

 -- Peter Pentchev <roam@debian.org>  Wed, 06 May 2020 23:27:44 +0300

id3tool (1.2a-11) unstable; urgency=medium

  * Declare compliance with Debian Policy 4.3.0 with no changes.
  * Bump the debhelper compatibility level to 12 with no changes.
  * Add the year 2019 to my debian/* copyright notice.

 -- Peter Pentchev <roam@debian.org>  Wed, 09 Jan 2019 01:53:06 +0200

id3tool (1.2a-10) unstable; urgency=medium

  * Use my Debian e-mail address.
  * Bring up to compliance with Debian Policy 4.2.1: install the upstream
    release notes (CHANGELOG) as NEWS and not changelog.
  * Drop the Lintian overrides related to B-D: debhelper-compat -
    Lintian 2.5.98 no longer emits these warnings and errors.
  * Fix several more typographical errors.

 -- Peter Pentchev <roam@debian.org>  Thu, 30 Aug 2018 15:48:39 +0300

id3tool (1.2a-9) unstable; urgency=low

  * Build-depend on debhelper 11 now that it's out, remove
    the Lintian override, and use the new B-D: debhelper-compat.
  * Declare compliance with Debian Policy 4.1.4 with no changes.
  * Remove some whitespace at EOL in an old changelog entry.
  * Add the year 2018 to my debian/* copyright notice.
  * Point the Vcs-* URLs to salsa.debian.org.

 -- Peter Pentchev <roam@ringlet.net>  Sun, 06 May 2018 16:15:23 +0300

id3tool (1.2a-8) unstable; urgency=medium

  * Use the HTTPS scheme for the copyright format specification URL.
  * Bump the year on my debian/* copyright notice.
  * Declare compliance with Debian Policy 4.1.1 with no changes.
  * Bump the debhelper compatibility level to 11 and the build dependency
    to 10.8~.
  * Add "Rules-Requires-Root: no" to the source control stanza.
  * Switch to git-dpm and rename the patches.
  * Fix some more typographical errors.
  * Mark all the patches as forwarded upstream (via private e-mail).

 -- Peter Pentchev <roam@ringlet.net>  Thu, 16 Nov 2017 17:14:51 +0200

id3tool (1.2a-7) unstable; urgency=medium

  * Declare compliance with Debian Policy 3.9.8 with no changes.
  * Switch the Vcs-Git URL to the HTTPS scheme.
  * Rename the "BSD-3" license to "BSD-3-clause" in the copyright file
    and bump the year on my debian/* copyright notice.
  * Update the watch file to version 4 and explicitly specify pgpmode=none.
  * Move the Large File Support to the 04-lfs configure.in patch.
  * Add an upstream metadata file.
  * Move the -Werror addition to the 05-werror configure.in patch.
  * Remove the debian/manpages file, upstream installs the manpage.
  * Bump the debhelper compatibility level to 10:
    - autoreconf and parallel are on by default now
    - override the Lintian debhelper version warning as it itself suggests

 -- Peter Pentchev <roam@ringlet.net>  Mon, 18 Apr 2016 14:46:51 +0300

id3tool (1.2a-6) unstable; urgency=medium

  * Bump Standards-Version to 3.9.6 with no changes.
  * Add the 03-autoreconf patch to modernize the autoconf templates.
  * Add Multi-Arch: foreign to the binary package.
  * Switch to the cgit frontend for Vcs-Browser.
  * Build with -Werror if the non-standard "werror" option is specified.

 -- Peter Pentchev <roam@ringlet.net>  Sat, 25 Oct 2014 01:03:13 +0300

id3tool (1.2a-5) unstable; urgency=medium

  * New maintainer.  Closes: #741277
  * Specify the 3.0 (quilt) source format.
  * Update the use of debhelper:
    - add misc:Depends to the binary package
    - bump the debhelper compatibility level to 9
    - let dh_clean take care of *-stamp
    - switch to debhelper override rules; this also no longer creates
      the .deps directory, so no need to remove it
    - minimize the rules file by using debian/docs and debian/manpages
  * Use all the build hardening flags provided by debhelper 9 and
    dpkg-buildflags.
  * Use dh-autoreconf to update the autoconf framework and also clean
    up all the autogenerated files.
  * Bump Standards-Version to 3.9.5:
    - build in parallel, not that it matters for this package
    - move the homepage from the description to a source control field
      Closes: #615422
    - add the Vcs-Git and Vcs-Browser source control fields
  * Update the copyright file:
    - convert it to the 1.0 copyright format
    - update the upstream copyright years
    - note that config.h.in is actually under GPL-2+, not BSD-3
    - add the Debian packaging copyright years
  * Add the 01-typo patch to fix the "Psychedelic" genre name.
  * Build with large file support.
  * Build with more compiler warnings and add the 02-warnings patch.

 -- Peter Pentchev <roam@ringlet.net>  Tue, 11 Mar 2014 16:25:56 +0200

id3tool (1.2a-4) unstable; urgency=low

  * Fixed handling of nostrip build option
    (Closes: #437195)

 -- Paul Cager <paul-debian@home.paulcager.org>  Sun, 12 Aug 2007 00:01:44 +0100

id3tool (1.2a-3) unstable; urgency=low

  * Migrate from experimental to unstable.

 -- Paul Cager <paul-debian@home.paulcager.org>  Sun, 15 Apr 2007 22:17:36 +0100

id3tool (1.2a-2) experimental; urgency=low

  * Package Description should say what an id3 tag is (Closes: #265150)
  * Uses deb_helper version 5

 -- Paul Cager <paul-debian@home.paulcager.org>  Thu, 22 Feb 2007 23:36:09 +0000

id3tool (1.2a-1) unstable; urgency=low

  * New maintainer (Closes: #400360).
  * New upstream release:
    - Option -c can only be first parameter (Closes: #280180).
  * Corrected spelling mistake (Closes: #363953).

 -- Paul Cager <paul-debian@home.paulcager.org>  Fri,  1 Dec 2006 23:09:54 +0000

id3tool (1.2-1) unstable; urgency=low

  * New upstream version
    - New licence
    (Closes: Bug#167828)

 -- Matt Hope <dopey@debian.org>  Mon, 18 Nov 2002 04:27:56 +1100

id3tool (1.1f-2) unstable; urgency=low

  * Changed from debstd to debhelper (v2)
  * Converted the package format to non-debian native
  * Prepared for upload to Debian. (Closes: #127624)
  * Updated information in debian/copyright

 -- Matt Hope <dopey@debian.org>  Mon, 18 Nov 2002 04:27:45 +1100

id3tool (1.1f-1) unstable; urgency=low

  * fix for an 'fseek bug' which I couldn't reproduce.
  * Fixed version numbers in the source, corrected email address, other misc
    documentation stuff.
  * really the last 1.1 release if I have anything to do with it.

 -- Chris Collins <xfire@xware.cx>  Wed, 5 Apr 2000 21:38:00 +1000

id3tool (1.1e) unstable; urgency=low

  * fix for a >128 byte file bug
  * last 1.1 release if I have anything to do with it.

 -- Chris Collins <ccollins@pcug.org.au>  Sat, 15 jan 2000 13:23:40 +1100

id3tool (1.1d) unstable; urgency=low

  * fixes for a few file handling bugs

 -- Chris Collins <ccollins@pcug.org.au>  Mon, 10 Jan 2000 22:09:50 +1100

id3tool (1.1c) unstable; urgency=low

  * Multiple file handling fix.
  * Source support for non-Unixy platforms.

 -- Chris Collins <ccollins@pcug.org.au>  Mon, 15 Nov 1999 20:54:00 +1100

id3tool (1.1b) unstable; urgency=low

  * Packaging Fix Release

 -- Chris Collins <ccollins@pcug.org.au>  Mon, 01 Nov 1999 10:15:00 +1000

id3tool (1.1a) unstable; urgency=low

  * Initial Release.

 -- Chris Collins <ccollins@pcug.org.au>  Sun, 19 Sep 1999 10:09:36 +1000
